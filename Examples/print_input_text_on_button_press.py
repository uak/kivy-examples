## Get text from TextInput on button press
## title: How to print value in a TextInput when a Button is pressed? (Kivi)
## source: https://stackoverflow.com/questions/46228969/how-to-print-value-in-a-textinput-when-a-button-is-pressed-kivi/46229263#46229263

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.lang import Builder

Builder.load_string("""

#:kivy 1.10.0

<Login_Screen>:
    BoxLayout:
        AnchorLayout:
            TextInput:
                id: user_input

    BoxLayout:
        AnchorLayout:
            TextInput:
                id: password_input

    BoxLayout:
        Button:
            id: login
            text: 'Login'
        Button:
            id: register
            text: 'Register'
            on_press: root.register()
""")



class Login_Screen(BoxLayout):

    def register(self):
        print("user: ", self.ids.user_input.text)
        print("password: ", self.ids.password_input.text)


class MainApp(App):

    def build(self):
        return Login_Screen()


if __name__ == '__main__':
    MainApp().run()
