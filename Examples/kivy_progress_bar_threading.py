## Progress bar or animation thread in Kivy
## Title: Cannot create graphics instruction outside the main Kivy thread
## source: https://stackoverflow.com/questions/71885469/cannot-create-graphics-instruction-outside-the-main-kivy-thread

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.progressbar import ProgressBar
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.lang import Builder

import time
import threading

Builder.load_string(""" 

<Interface>:
    orientation: 'vertical'
    Label:
        text: "Test"

    BoxLayout:
        orientation: 'vertical'
        Label
            text: "phone Number"

        TextInput:
            id: variable
            hint_text: ""
        Thebutton:
            user_input: variable.text
            text: "Buttion"
            on_release: self.do_action()

""")


class Interface(BoxLayout):
    pass


class Thebutton(Button):

    def bar_de_progress(self):
        # this is run on the main thread
        self.bdp = ProgressBar()
        poo = Popup(title="Brute Forcing ...", content=self.bdp, size_hint=(0.5, 0.2))
        poo.open()
        threading.Thread(target=self.update_progress).start()

    def update_progress(self):
        # this is run on another thread
        time.sleep(1)
        self.bdp.value = 25
        time.sleep(1)
        self.bdp.value = 50
        time.sleep(1)
        self.bdp.value = 75
        time.sleep(1)
        self.bdp.value = 100


    def do_action(self, *args):
        self.bar_de_progress()


class MyApp(App, Thebutton):

    def build(self):
        return Interface()


if __name__ == "__main__":
  MyApp().run()
