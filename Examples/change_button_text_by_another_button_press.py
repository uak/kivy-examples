## Change a button text by another button click
## title: How can I make in Kivy by clicking a button to change the text of another button(buttons)?
## source: https://stackoverflow.com/questions/59605479/how-can-i-make-in-kivy-by-clicking-a-button-to-change-the-text-of-another-button

from kivy.app import App
from kivy.lang.builder import Builder
from kivy.properties import StringProperty
from kivy.uix.boxlayout import BoxLayout


class Container(BoxLayout):
    message = StringProperty()

    def retranslate(self, language):
        texts = {"en": "Hello World", "fr": "Salut monde"}
        self.message = texts.get(language, "")


Builder.load_string(
    """
<Container>:
    orientation: 'vertical'
    Button:
        text: root.message
    Button:
        text: "Eng"
        on_press: root.retranslate("en")
    Button:
        text: "Fra"
        on_press: root.retranslate("fr")
"""
)


class MyApp(App):
    def build(self):
        w = Container()
        w.retranslate("en")
        return w


if __name__ == "__main__":
    MyApp().run()
